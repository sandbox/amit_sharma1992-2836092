<?php

namespace Drupal\imagefield_osslider\Plugin\Field\FieldFormatter;

use Drupal\image\Plugin\Field\FieldFormatter\ImageFormatterBase;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Url;
use Drupal\image\Entity\ImageStyle;
use Drupal\Core\Utility\LinkGeneratorInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Plugin implementation of the 'osslider_image_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "osslider_image_formatter",
 *   label = @Translation("Osslider image formatter"),
 *   field_types = {
 *     "image"
 *   }
 * )
 */
class OssliderImageFormatter extends ImageFormatterBase implements ContainerFactoryPluginInterface {
  /**
  * The current user.
  *
  * @var \Drupal\Core\Session\AccountInterface
  */
  protected $currentUser;

  /**
  * The image style entity storage.
  *
  * @var \Drupal\Core\Entity\EntityStorageInterface
  */
  protected $imageStyleStorage;

  /**
  * Constructs an ImageFormatter object.
  *
  * @param string $plugin_id
  *   The plugin_id for the formatter.
  * @param mixed $plugin_definition
  *   The plugin implementation definition.
  * @param \Drupal\Core\Field\FieldDefinitionInterface $field_definition
  *   The definition of the field to which the formatter is associated.
  * @param array $settings
  *   The formatter settings.
  * @param string $label
  *   The formatter label display setting.
  * @param string $view_mode
  *   The view mode.
  * @param array $third_party_settings
  *   Any third party settings settings.
  * @param \Drupal\Core\Session\AccountInterface $current_user
  *   The current user.
  * @param \Drupal\Core\Utility\LinkGeneratorInterface $link_generator
  *   The link generator service.
  * @param \Drupal\Core\Entity\EntityStorageInterface $image_style_storage
  *   The entity storage for the image.
  */
  public function __construct($plugin_id, $plugin_definition, FieldDefinitionInterface $field_definition, array $settings, $label, $view_mode, array $third_party_settings, AccountInterface $current_user, LinkGeneratorInterface $link_generator, EntityStorageInterface $image_style_storage) {
    parent::__construct($plugin_id, $plugin_definition, $field_definition, $settings, $label, $view_mode, $third_party_settings);
    $this->currentUser = $current_user;
    $this->imageStyleStorage = $image_style_storage;
  }

  /**
  * {@inheritdoc}
  */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $plugin_id,
      $plugin_definition,
      $configuration['field_definition'],
      $configuration['settings'],
      $configuration['label'],
      $configuration['view_mode'],
      $configuration['third_party_settings'],
      $container->get('current_user'),
      $container->get('link_generator'),
      $container->get('entity.manager')->getStorage('image_style')
    );
  }

  /**
  * {@inheritdoc}
  */
  public static function defaultSettings() {
    return array(
      'image_style' => '',
      'imagefield_osslider_speed' => 3000,
      'imagefield_osslider_autoplay' => '',
    ) + parent::defaultSettings();
  }

  /**
  * {@inheritdoc}
  */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $image_styles = image_style_options(FALSE);
    $element['image_style'] = array(
      '#title' => t('Image style'),
      '#type' => 'select',
      '#default_value' => $this->getSetting('image_style'),
      '#empty_option' => t('None (original image)'),
      '#options' => $image_styles,
      '#description' => array(
        '#markup' =>t('Select Image style'),
        '#access' => $this->currentUser->hasPermission('administer image styles'),
      ),
    );

    $element['imagefield_osslider_speed'] = [
      '#type' => 'number',
      '#title' => t('Slider Speed'),
      '#default_value' => $this->getSetting('imagefield_osslider_speed'),
      '#description' => t('The speed of the animation.'),
    ];

    $element['imagefield_osslider_autoplay'] = [
      '#type' => 'checkbox',
      '#title' => t('Slider Autoplay'),
      '#default_value' => $this->getSetting('imagefield_osslider_autoplay'),
      '#description' => t('Is autoplay?.'),
    ];

    return $element;
  }

  /**
  * {@inheritdoc}
  */
  public function settingsSummary() {
    $summary = array();
    $image_styles = image_style_options(FALSE);

    // Unset possible 'No defined styles' option.
    unset($image_styles['']);

    // Styles could be lost because of enabled/disabled modules that defines
    // their styles in code.
    $image_style_setting = $this->getSetting('image_style');
    if (isset($image_styles[$image_style_setting])) {
     $summary[] = t('Image style: @style', array('@style' => $image_styles[$image_style_setting]));
    } else {
      $summary[] = t('Original image');
    }

    // Slider speed.
    $osslider_speed = $this->getSetting('imagefield_osslider_speed');
    if (isset($osslider_speed)) {
      $summary[] = t('Speed of slider: @speed', array('@speed' => $osslider_speed));
    }

    // Autoplay setting.
    $osslider_autoplay = $this->getSetting('imagefield_osslider_autoplay');
    if (isset($osslider_autoplay)) {
      $summary[] = t('Autoplay: @autoplay', array('@autoplay' => t('TRUE')));
    }

    return $summary;
  }

  /**
  * {@inheritdoc}
  */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    $elements = array();
    $imagefield_osslider_image_style_setting = $this->getSetting('image_style');

    $osslider_image_style = NULL;
    if (!empty($imagefield_osslider_image_style_setting)) {
      $osslider_image_style = entity_load('image_style', $imagefield_osslider_image_style_setting);
    }

    $image_uri_values = [];
    foreach ($items as $delta => $item) {
      if ($item->entity) {
        $image_uri = $item->entity->getFileUri();

        // Get absolute path for orignial path
        $orignial_absolute_file_path = $item->entity->url();

        // Get thumb image style URL
        if ($osslider_image_style) {
          $thumb_image_uri = ImageStyle::load($osslider_image_style->getName())->buildUrl($image_uri);
        } else {
          $thumb_image_uri = $orignial_absolute_file_path;
        }

        $image_uri_values[] = $thumb_image_uri;
      }
    }

    if (count($image_uri_values) > 0) {
      $elements[] = array (
        '#theme' => 'osslider',
        '#items' => $image_uri_values,
        '#field_name' => str_replace("_", "-", $this->fieldDefinition->getName()),
      );

      // Attach the image popup library
      $elements['#attached']['library'][] = 'imagefield_osslider/osslider';

      // Attach settings to js
      $drupalSettings = [
        'field_name' => str_replace("_", "-", $this->fieldDefinition->getName()),
        'speed' => $this->getSetting('imagefield_osslider_speed'),
        'autoplay' => !empty($this->getSetting('imagefield_osslider_autoplay')) ? true : false,
      ];
      $elements['#attached']['drupalSettings']['imagefield_osslider'] = $drupalSettings;
    }

    return $elements;
  }

}
