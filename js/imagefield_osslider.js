/**
 * @file
 * Defines Javascript behaviors for the imagefield_slideshow module.
 */

(function ($, Drupal, drupalSettings) {

  'use strict';

  $(document).ready(function () {

    var slider = new osSlider({
      pNode:'.' + drupalSettings.imagefield_osslider.field_name,
      cNode:'.'  + drupalSettings.imagefield_osslider.field_name + '-main li',
      speed: drupalSettings.imagefield_osslider.speed,
      autoPlay: drupalSettings.imagefield_osslider.autoplay
    });

  });

})(jQuery, Drupal, drupalSettings);
